import React, { useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import FilterSizes from './FilterSizes'
import Product from './Product'
import { filterProducts } from '../features/productsSlice'

const DisplayProducts = () => {

  const { products, selectedSizes } = useSelector(store => store.products)

  let filteredData = []
  selectedSizes.map(eachSize => {
    products.map(eachProduct => {
      if (eachProduct.availableSizes.includes(eachSize)) {
        if (!filteredData.includes(eachProduct)) {
          filteredData.unshift(eachProduct)
        }
      }
    })
  })


  return (
    <section className='d-flex mt-5 ps-5 w-100'>
      <FilterSizes />
      <div className='d-flex flex-column'>
        <div className="d-flex">
          <h4 className='ms-5'>{filteredData.length != 0 ? filteredData.length : products.length}{` Product(s) found`}</h4>
        </div>
        <div className="d-flex flex-row flex-wrap ps-5">
          {
            (filteredData.length != 0 ? filteredData : products).map(eachProduct => {
              return <Product productDetails={eachProduct} key={eachProduct.id} />
            })
          }
        </div>
      </div>
    </section>
  )
}

export default DisplayProducts