import { useDispatch, useSelector } from 'react-redux'
import cartImage from '../assets/bag-icon.png'
import { showHideCart } from '../features/productsSlice'
import Cart from './Cart'
import { useEffect } from 'react'
import { calculateAmount } from '../features/cartSlice'


function Modal() {

    const { showCartBoard } = useSelector(store => store.products)
    const { itemsCount, cartItems, totalAmount } = useSelector(store => store.cart)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(calculateAmount())
    }, [itemsCount])

    return (
        <div className=''>
            <button
                className="btn btn-dark"
                type="button"
                style={{ width: showCartBoard ? '34rem' : '' }}
                onClick={() => dispatch(showHideCart())}
            >
                <h4 className='text-warning text-start'>
                    {!showCartBoard ? <img src={cartImage} alt="cart" /> : 'X'}{!showCartBoard && itemsCount}
                </h4>
            </button>
            <div
                className={`offcanvas offcanvas-end ${showCartBoard ? 'show' : ''} text bg-dark`}
                data-bs-scroll="true"
                data-bs-backdrop="false"
                tabIndex={-1}
                id="offcanvasScrolling"
                aria-labelledby="offcanvasScrollingLabel"
                style={{ width: '27%' }}
            >
                <div className="offcanvas-header">

                </div>
                <div className="offcanvas-body text text-light">
                    <Cart />
                </div>
                <div className="shadow border-top border-secondary offcanvas-footer text text-light pb-3 bg-dark d-flex flex-column justify-content-center align-items-center">
                    <div className='d-flex flex-row justify-content-between p-3 w-100'>
                        <h5 style={{ color: 'gray' }}>Subtotal</h5>
                        <div className='d-flex flex-column'>
                            <h3 className='text-warning'>$ {totalAmount}</h3>
                            <h6 className='text-secondary'>{cartItems.length === 0 ? '' : `OR UP TO 5 X ${(totalAmount / 5).toFixed(2)}`}</h6>
                        </div>
                    </div>
                    <button type='button' className='btn btn-outline-secondary w-75' onClick={() => alert(`total amount $ ${totalAmount}`)}>Checkout</button>
                </div>
            </div>
        </div >
    )
}

export default Modal