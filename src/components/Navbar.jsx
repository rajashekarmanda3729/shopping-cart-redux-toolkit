import { useDispatch } from "react-redux"
import { showHideCart } from "../features/productsSlice"
import Modal from './Modal'


function Navbar() {

    const dispatch = useDispatch()

    return (
        <div className='d-flex flex-row justify-content-between bg-light'>
            <img src='https://cdn.discordapp.com/attachments/1072061762536493137/1097450744652775515/githubicon.png'
              className=""  alt='github' style={{ height: '3.5rem' }} onClick={() => dispatch(showHideCart())} />
            <Modal />
        </div>
    )
}

export default Navbar