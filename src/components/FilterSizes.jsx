import React from 'react'
import { useSelector } from 'react-redux'
import FilterBy from './FilterBy'

const filterSizes = () => {
    
    const { sizes } = useSelector(store => store.products)
    
    return (
        <div className='d-flex flex-column m-2'>
            <h3>Sizes:</h3>
            <div className="d-flex flex-row flex-wrap" style={{width:'200px'}}>
                {
                    sizes && sizes.map(size => {
                        return <FilterBy filterSize={size} key={size} />
                    })
                }
            </div>
        </div>
    )
}

export default filterSizes