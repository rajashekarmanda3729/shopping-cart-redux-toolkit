import React from 'react'
import { useDispatch } from 'react-redux'
import { showCart } from '../features/productsSlice'
import { addToCart } from '../features/cartSlice'

const Product = ({ productDetails }) => {

    const dispatch = useDispatch()

    const { id,
        price,
        installments,
        isFreeShipping,
        title,
        url,
        quantitiy,
    } = productDetails

    return (
        <div className="d-flex flex-column shadow position-relative border m-3" style={{ width: '18rem', minHeight: '30rem' }}>
            <img src={url} className="card-img-top mb-3" alt={title} />
            {
                isFreeShipping && <button className="btn btn-dark btn-sm position-absolute top-0 end-0">free shipping</button>

            }
            <div className="d-flex flex-column justify-content-center align-items-center">
                <h5 className='mt-2 mb-0'>{title}</h5>
                <hr className='border border-warning border-1 w-25' />
                <h4 className='mt-2'>$ {price}</h4>
                <h5 className='mb-5 mt-1'>or {installments} X {(price / installments).toFixed(2)}</h5>
                <button className="btn btn-dark w-100 position-absolute bottom-0"
                    onClick={() => {
                        dispatch(addToCart(productDetails))
                        dispatch(showCart())
                    }}>Add to cart</button>
            </div >
        </div >

    )
}

export default Product