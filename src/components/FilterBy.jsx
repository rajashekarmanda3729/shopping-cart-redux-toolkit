import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { selectedSize } from '../features/productsSlice'

const FilterBy = ({ filterSize }) => {

    const dispatch = useDispatch()
    const { selectedSizes } = useSelector(store => store.products)

    return (
        <button className={`btn ${selectedSizes.includes(filterSize) ? 'btn-dark' : 'btn-light'} rounded-circle m-1`}
            style={{ width: '3rem', height: '3rem' }}
            onClick={() => dispatch(selectedSize(filterSize))}>
            {filterSize}
        </button>
    )
}

export default FilterBy