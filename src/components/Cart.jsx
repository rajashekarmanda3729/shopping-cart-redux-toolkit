import { useSelector } from 'react-redux'
import cartImage from '../assets/bag-icon.png'
import CartItem from './CartItem'

function Cart() {

    const { cartItems, itemsCount } = useSelector(store => store.cart)

    return (
        <div className='d-flex flex-column'>
            <h5 className='text text-warning text-center mt-3'>
                <img src={cartImage} alt="cart" />{itemsCount} <span className='text-light'>Cart</span>
            </h5>
            {
                cartItems && cartItems.map(eachItem => {
                    return <CartItem key={eachItem.id} itemDetails={eachItem} />
                })
            }
        </div>
    )
}

export default Cart