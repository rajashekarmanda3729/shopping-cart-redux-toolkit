import { useDispatch } from "react-redux"
import { onIncrementQuantity, onRemoveProduct, addToCart, onDecrementQuantirty } from "../features/cartSlice"

function CartItem({ itemDetails }) {

    const dispatch = useDispatch()

    const {
        title,
        availableSizes,
        price,
        sku,
        url,
        quantity
    } = itemDetails

    return (
        <div className="card mb-3 bg bg-dark p-1" style={{ maxWidth: '540px', height: '7rem' }}>
            <div className="row g-0">
                <div className="col-md-2">
                    <img src={url} className="img-fluid rounded-start w-75" alt={title} />
                </div>
                <div className="col-md-8">
                    <div className="card-body">
                        <h5 className="card-title text-secondary">{title}</h5>
                        <p className="card-text text-secondary">{availableSizes && availableSizes[0]} | Q:{quantity}</p>
                    </div>
                </div>
                <div className="d-flex flex-column col-md-2 text-dark">
                    <button className='btn btn-secondary w-50 h-50 bg-transparent border border-0 text-secondary ms-5'
                        onClick={() => dispatch(onRemoveProduct(itemDetails))}>X</button>
                    <p className='mb-0 text-warning text-sm'>$ {price}</p>
                    <div className='mt-0'>
                        <button type='button' className='btn btn-outline-secondary btn-sm m-1'
                            onClick={() => {
                                quantity !== 1 && dispatch(onDecrementQuantirty(itemDetails))
                            }}>-</button>
                        <button type='button' className='btn btn-outline-secondary btn-sm m-1'
                            onClick={() => dispatch(onIncrementQuantity(itemDetails))}>+</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CartItem