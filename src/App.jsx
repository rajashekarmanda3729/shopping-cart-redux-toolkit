import Navbar from './components/Navbar'
import DisplayProducts from './components/DisplayProducts'

function App() {

  return (
    <main className='d-flex flex-column'>
      <Navbar />
      <DisplayProducts />
    </main>
  )
}

export default App
