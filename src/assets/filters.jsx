export const sizes = [
    {
        id: 1,
        size: 'XS',
        selected: false
    },
    {
        id: 1,
        size: 'S',
        selected: false
    },
    {
        id: 1,
        size: 'M',
        selected: false
    },
    {
        id: 1,
        size: 'ML',
        selected: false
    },
    {
        id: 1,
        size: 'L',
        selected: false
    },
    {
        id: 1,
        size: 'XL',
        selected: false
    },
    {
        id: 1,
        size: 'XXL',
        selected: false
    }
]