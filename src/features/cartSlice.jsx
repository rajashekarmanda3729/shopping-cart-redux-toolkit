import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    cartItems: [],
    totalAmount: 0,
    itemsCount: 0,
    cartIds: []
}

export const cartSlice = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        addToCart: (state, { payload }) => {
            let isFindItem = false
            const findItem = state.cartItems.find(e => e.id === payload.id)
            state.cartItems.map(each => {
                if (each.id === payload.id) isFindItem = true
            })
            if (isFindItem) {
                const obj = {
                    ...payload,
                    quantity: findItem.quantity + 1
                }
                const filterItems = state.cartItems.filter(e => e.id !== payload.id)
                state.cartItems = [...filterItems, obj]
                state.itemsCount++
            } else {
                state.cartItems = [...state.cartItems, { ...payload, quantity: payload.quantity + 1 }]
                state.itemsCount++
            }
        },
        calculateAmount: (state) => {
            let amount = 0
            state.cartItems.map(each => {
                amount += each.quantity * each.price
            })
            state.totalAmount = amount.toFixed(2)
        },
        onRemoveProduct: (state, { payload }) => {
            const filterProducts = state.cartItems.filter(product => product.id !== payload.id)
            state.cartItems = filterProducts
        },
        onIncrementQuantity: (state, { payload }) => {
            const filterItems = state.cartItems.map(product => {
                if (product.id === payload.id) {
                    return {
                        ...product,
                        quantity: product.quantity + 1
                    }
                } else {
                    return product
                }
            })
            state.cartItems = filterItems
            state.itemsCount++
        },
        onDecrementQuantirty: (state, { payload }) => {
            const filterItems = state.cartItems.map(product => {
                if (product.id === payload.id) {
                    return {
                        ...product,
                        quantity: product.quantity - 1
                    }
                } else {
                    return product
                }
            })
            state.cartItems = filterItems
            state.itemsCount--
        }
    }
})

export const { addToCart, calculateAmount, onRemoveProduct,
    onIncrementQuantity, onDecrementQuantirty } = cartSlice.actions

export default cartSlice.reducer