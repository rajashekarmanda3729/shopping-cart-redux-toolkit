import { createSlice } from "@reduxjs/toolkit";
import productsData from '../assets/data.json'
import { sizesArr } from '../assets/filtersSizes'

const initialState = {
    products: productsData.products,
    sizes: sizesArr,
    selectedSizes: [],
    showCartBoard: false
}

export const productsSlice = createSlice({
    name: 'products',
    initialState,
    reducers: {
        selectedSize: (state, action) => {
            if (!state.selectedSizes.includes(action.payload)) {
                state.selectedSizes = [...state.selectedSizes, action.payload]
            } else {
                const filter = state.selectedSizes.filter(size => size !== action.payload)
                state.selectedSizes = filter
            }
        },
        showHideCart: (state) => {
            state.showCartBoard = !state.showCartBoard
        },
        showCart: (state) => {
            state.showCartBoard = true
        }
    }
})

export const { selectedSize, filterProducts, showHideCart,
    showCart } = productsSlice.actions

export default productsSlice.reducer